from keycloak import KeycloakOpenID

from config import config
from utls import logger

logger.debug(f"server_url ==>  {config['auth']['server_url']}")
logger.debug(f"client_id ==>  {config['auth']['client_id']}")
logger.debug(f"realm_name ==>  {config['auth']['realm_name']}")

keycloak_openid = KeycloakOpenID(server_url=config['auth']['server_url'],
                                 client_id=config['auth']['client_id'],
                                 realm_name=config['auth']['realm_name']
                                 )

# config_well_know = keycloak_openid.well_know()

def userInfo(token):
    try:
        return keycloak_openid.userinfo(token)
    except Exception as err:
        logger.error(err)
        return None

def valid_token(token):
    token_data = userInfo(token)
    if token_data:
        return True
    return False