from flask import Flask, request, json
from flask_restful import Api
from flask_cors import CORS
from pony.flask import Pony

from config import config
from auth import userInfo, valid_token
from resources import Welcome, Projects, Uploads, Library, Result, Project, Bom
from utls import logger

app = Flask(__name__)
CORS(app)
api = Api(app)

app.config.update(
    dict(PONY=config['database'])
)

api.add_resource(Welcome, "/")
api.add_resource(Projects, "/projects")
api.add_resource(Project, "/project/<string:uuid>")
api.add_resource(Uploads, "/upload")
api.add_resource(Library, "/library")
api.add_resource(Result, "/result")
api.add_resource(Bom, "/bom")

# @app.before_request
# def auth_user():
#     if request.method != "OPTIONS":
#         token = request.headers.get('Authorization')
#         if valid_token(token):
#             logger.info("Token is valid")
#         else:
#             data = {"message": "Access Forbidden 403"}
#             response = app.response_class(
#                 response=json.dumps(data),
#                 mimetype='application/json',
#                 status=403
#             )
#             return response
#
#     else:
#         logger.info("OPTIONS was used")

Pony(app)
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')