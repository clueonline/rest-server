FROM python:3.8

EXPOSE 5000

RUN mkdir /app
WORKDIR /app

COPY requriments.txt /app

RUN pip install -r requriments.txt

COPY . /app

CMD python app.py