import uuid

def does_exist(data, value):
    try:
        test = data[value]
        return True
    except KeyError:
        return False

def get_uuid():
    return uuid.uuid4().hex


def check_is_string(string):
    try:
        value = string.strip()

        if len(value) > 0:
            return value
        else:
            return None
    except AttributeError as e:
        print(e)
        return None


def check_is_float(num):

    value = check_is_string(num)

    if isFloat(value):
        return float(value)
    else:
        return None

def check_is_int(num):

    value = check_is_string(num)

    if isInt(value):
        return int(value)
    else:
        return None

def isFloat(num):
    try:
        float(str(num))
        return True
    except ValueError:
        return False


def isInt(num):
    try:
        int(str(num))
        return True
    except ValueError:
        return False

def toInt(num_as_str):
    if isInt(num_as_str):
        return int(num_as_str)
    else:
        return None