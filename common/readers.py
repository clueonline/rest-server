from io import StringIO

from common.utls import check_is_string, check_is_float, check_is_int
from models.model import BomFile, BomFileContent, db
from csv import DictReader


class RawBomFile:

    ITEM_NO = "ITEM NO."
    PART_NUMBER = "PART NUMBER"
    DESCRIPTION = "DESCRIPTION"
    BB_LENGTH = "3D-Bounding Box Length"
    BB_WIDTH = "3D-Bounding Box Width"
    BB_THICKNESS = "3D-Bounding Box Thickness"
    LENGTH = "LENGTH"
    QTY = "QTY."

    def __init__(self, file_data, bom_file):
        self.file_data: str = file_data
        self.bom_file: BomFile = bom_file

    def run(self):
        output = StringIO(self.file_data)

        data = DictReader(output)
        for row in data:
            value = self._convert_data(row)
            self._create_bom_file_content(value)

        db.commit()

    def _convert_data(self, data):
        data[self.ITEM_NO] = check_is_string(data[self.ITEM_NO])
        data[self.PART_NUMBER] = check_is_string(data[self.PART_NUMBER])

        data[self.DESCRIPTION] = check_is_string(data[self.DESCRIPTION])

        try:
            data[self.BB_LENGTH] = check_is_float(data[self.BB_LENGTH])
        except KeyError:
            data[self.BB_LENGTH] = None

        try:
            data[self.BB_WIDTH] = check_is_float(data[self.BB_WIDTH])
        except KeyError:
            data[self.BB_WIDTH] = None

        try:
            data[self.BB_THICKNESS] = check_is_float(data[self.BB_THICKNESS])
        except KeyError:
            data[self.BB_THICKNESS] = None

        data[self.LENGTH] = check_is_float(data[self.LENGTH])

        data[self.QTY] = check_is_int(data[self.QTY])

        return data

    def _create_bom_file_content(self, value):
        bom_file_content = BomFileContent(bom_file=self.bom_file)

        bom_file_content.item_no = value[self.ITEM_NO]
        if value[self.PART_NUMBER] is not None:
            bom_file_content.part_number = value[self.PART_NUMBER]

        if value[self.DESCRIPTION] is not None:
            bom_file_content.description = value[self.DESCRIPTION]

        try:
            bom_file_content.BB_length = value[self.BB_LENGTH]
        except KeyError:
            bom_file_content.BB_length = None

        try:
            bom_file_content.BB_width = value[self.BB_WIDTH]
        except KeyError:
            bom_file_content.BB_width = None

        try:
            bom_file_content.BB_thickness = value[self.BB_THICKNESS]
        except KeyError:
            bom_file_content.BB_thickness = None

        bom_file_content.length = value[self.LENGTH]
        bom_file_content.qty = value[self.QTY]
        bom_file_content.parent = None
