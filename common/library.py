from pony.orm import select

from common.utls import get_uuid
from models.model import BomFile, Library_beam_alias


def get_file_materials(file_uuid):
    bom_file: BomFile = select(b for b in BomFile if b.uuid == file_uuid).first()
    materials = bom_file.materials_used()
    return materials


def get_library_materials(material_list):
    beams = select(x.beam for x in Library_beam_alias if x.description in material_list)
    if beams.count() > 0:
        return beams[:]
    return None


def create_missing_materials(material_list):
    materials = []
    for material in material_list:
        materials.append({
            'name': material,
            'id': f'temp{get_uuid()}',
            'status': 'missing',
            'lengths': []
        })
    return materials

def combined_materials(existing_materials_list, missing_materials_list):
    have = []
    for material in existing_materials_list:
        for alias in material.aliases:
            if alias.description in missing_materials_list:
                have.append(alias.description)
    need = list(set(missing_materials_list) - set(have))
    missing = create_missing_materials(need)
    existing = library_materials_to_dict(existing_materials_list)
    all_materials = existing + missing
    return all_materials

def library_materials_to_dict(library_materials):
    output = []
    for material in library_materials:
        output.append(material.to_dict())
    output = adjust_lengths(output)
    return output

def adjust_lengths(library_materials):
    for material in library_materials:
        for length in material['lengths']:
            length['id'] = f'{material["id"]}-{length["id"]}'
            length['selected'] = True
    return library_materials
