import os

from utls import logger

if os.environ.get("DEPLOY") == "production":
    config = {
        'database': {
            "provider": os.environ.get('PROVIDER'),
            "user": os.environ.get('USER'),
            "password": os.environ.get('PASSWORD'),
            "host": os.environ.get('HOST'),
            "port": os.environ.get('PORT'),
            "database": os.environ.get('DATABASE'),
            },
        'auth': {
            "server_url": os.environ.get('SERVER_URL'),
            "client_id": os.environ.get('CLIENT_ID'),
            "realm_name": os.environ.get('REALM_NAME')
        }
    }
else:
    config = {
        'database': {
            "provider": "sqlite",
            "filename": "db.db3",
            "create_db": True,
        },
        'auth': {
            "server_url": "http://0.0.0.0:8080/auth/",
            "client_id": "api-test-app",
            "realm_name": "keycloak-demo"
        }
    }

logger.debug(config)