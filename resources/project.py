from flask import request
from flask_restful import Resource
from pony.orm import select, db_session
from models.model import Project as db_Project


class Project(Resource):

    @db_session
    def get(self, uuid):
        responses = {}
        project = select(project for project in db_Project if project.uuid == uuid).first()
        resource = request.args.get('resource')

        if resource == 'header':
            responses['header'] = project.get_header()
        elif resource == 'stats':
            responses['stats'] = project.get_stats()
        elif resource == 'boms':
            responses['boms'] = project.get_boms()
        # {
        #     key: uuid
        #     created: str
        #     description: str
        #     tags: []
        # }
        return responses