from flask import request
from flask_restful import Resource
from pony.orm import db_session

from common.readers import RawBomFile
from common.utls import get_uuid
from models.model import BomFile, db


class Uploads(Resource):

    @db_session
    def post(self):
        data = {
            'filename': '',
            'project_id': '',
            'data': ''
        }
        project_id = get_project_id(request.values.dicts)
        file_data = request.files.get('file')
        data['filename'] = file_data.filename
        data['data'] = file_data.read().decode('ISO-8859-1')
        data['project_id'] = project_id
        
        record_id = save_file_data_db(data)
        
        return record_id


def get_project_id(dicts):
    for d in dicts:
        data = d.get('project_id')
        if data is not None:
            return data
    return None

def save_file_data_db(data):
    bom_file = BomFile(project_id=data['project_id'],
                       filename=data['filename'],
                       uuid=get_uuid())
    db.commit()

    save_file_contents(data['data'], bom_file)

    return bom_file.uuid

def save_file_contents(data, bom_file):
    raw_bom_file = RawBomFile(data, bom_file)
    raw_bom_file.run()