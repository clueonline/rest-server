from operator import itemgetter

from flask import request, json
from flask_restful import Resource
from pony.orm import db_session, select

from common.library import get_file_materials, get_library_materials, create_missing_materials, combined_materials, \
    library_materials_to_dict
from common.utls import get_uuid, toInt
from models.model import Library_beam, Library_beam_alias, Library_beam_length, db

class Library(Resource):

    def get(self):
        resource = request.args.get('resource')
        if resource == 'display':
            print("I did get to this point")
            result = library_data()
            results = {'data': result}
        else:
            file_uuid = request.args.get('file_uuid')
            file_materials = get_file_materials(file_uuid)
            library_materials = get_library_materials(file_materials)

            if library_materials is not None and len(library_materials) == len(file_materials):
                results = library_materials_to_dict(library_materials)
            elif library_materials is None:
                results = create_missing_materials(file_materials)
            else:
                results = combined_materials(library_materials, file_materials)
        return results

    @db_session
    def post(self):
        data = json.loads(request.data)
        add_materials_to_Library(data['data'])
        print("I So Got Called :)")
        return {'message': 'You posted the data'}

def add_materials_to_Library(data):
    for item in data:
        display_name = item['name']
        description = item['name']
        beam_uuid = get_uuid()
        sizes = []
        for length in item['lengths']:
            if length['save']:
                sizes.append({
                    'uuid': get_uuid(),
                    'length': toInt(length['name'])
                })
        if len(sizes) > 0:
            beam: Library_beam = Library_beam(uuid=beam_uuid, display_name=display_name)
            beam.aliases.create(description=description)
            for size in sizes:
                beam.lengths.create(uuid=size['uuid'], length=size['length'])
            db.commit()

def library_data():
    result_tmp = select(beam for beam in Library_beam)
    result = []
    for tmp in result_tmp:
        result.append(tmp.for_display())
    result = sorted(result, key=itemgetter('name'))
    return result