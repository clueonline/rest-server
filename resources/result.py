import datetime
from operator import itemgetter
from time import sleep

from flask import request, json
from flask_restful import Resource
from pony.orm import select, db_session

from common.utls import get_uuid
from models.model import Project, BomFile, BomResult, db, BomResultSection, BomResultBeam


class Result(Resource):

    @db_session
    def get(self):
        uuid = request.args.get('result')
        result = select(b for b in BomResult if b.uuid == uuid).first()
        result = result.to_dict()
        return {'result': result}

    @db_session
    def post(self):
        # desc: {
        #     uuid: '',
        #     id: '',
        #     project: '',
        #     client: '',
        #     date: `${date.toDateString()} ${date.toTimeString()}
        # `,
        # description: '',
        # filename: '',
        # file_id: '',
        # beams: [],
        # missing_beams: [],
        # process_start: date.getTime(),
        # process_finish: null
        # }

        data = json.loads(request.data)['data']
        print(data)

        result_id = process(data)
        return {"result_id": result_id}

    @db_session
    def patch(self):
        data = json.loads(request.data)
        result_uuid = data['result_uuid']
        process_request = data['request']
        process_data = data['data']
        result = patch_process(result_uuid, process_request, process_data)

        return {"result": result}

def patch_process(uuid, process_request, data):
    result = 'Error'

    if process_request == 'update_processing_time':
        result = update_processing_time(uuid, data)

    return result


def update_processing_time(uuid,  data):
    bom_result: BomResult = select(b for b in BomResult if b.uuid == uuid).first()

    if bom_result.process_time is None:
        bom_result.process_time = datetime.datetime.fromtimestamp(data['process_finish'] / 1000) - bom_result.timestamp
        db.commit()
        return 'Update complete'
    else:
        return "Value already set"


def process(data):
    # phase 1
    # find the project
    project = select(p for p in Project if p.uuid == data['uuid'] ).first()
    bom_file = select(b for b in BomFile if b.uuid == data['file_id']).first()
    # create a new results object
    bom_result: BomResult = BomResult(project=project, bom_file=bom_file)
    # store the json format of the config

    bom_result.uuid = get_uuid()
    bom_result.filename = data['filename']
    bom_result.description = data['description']
    bom_result.timestamp = datetime.datetime.fromtimestamp(data['process_start']/1000)
    bom_result.config = json.dumps(data)

    # phase 2
    bom_result.bom_file.set_items_parents()
    config_data = setup_data_required(bom_result)

    # phase 3
    beams, missing_items = generate_result(config_data)

    save_results(bom_result, beams, missing_items)


    # return a result ID
    db.commit()
    return bom_result.uuid

def save_results(result:BomResult = None, beams=None, missing_items=None):
    sections = []

    for material in beams.keys():
        sections.append(BomResultSection(name=material, bom_result=result))

    for material in missing_items.keys():
        not_found = True
        for section in sections:
            if section.name == material:
                not_found = False
                continue
        if not_found:
            sections.append(BomResultSection(name=material, bom_result=result))

    for section in sections:
        result_beams = beams.get(section.name)
        if result_beams is not None:
            for beam in result_beams:
                db_beam = BomResultBeam(qty=beam.qty, length=int(beam.length), waste=int(beam.waste), section=section)
                for item in beam.items:
                    db_beam.parts.create(item_no=item['item_no'], length=item['length'], qty=item['qty'])

        missing = missing_items.get(section.name)
        if missing is not None:
            for item in missing:
                section.missing_parts.create(item_no=item['item_no'], length=item['length'], qty=item['qty'])



def  generate_result(config_data):

    # missing_items = { material(str):[
    #     {
    #         'item_no': str,
    #         'qty': int,
    #         'length': int,
    #         'material': str
    #     }
    # ]
    # }

    # beams_used = { material(str): [
    #     {
    #         'material': str,
    #         'length': int,
    #         'waste': int,
    #         'qty': int,
    #         'items': [
    #             {
    #                 'item_no': str,
    #                 'qty': int,
    #                 'length': int
    #             }
    #         ]
    #     }
    # ]
    # }

    missing_items = {}
    beams_used = {}

    print(config_data)
    for material in config_data.keys():
        missing_items[material] = []
        beams_used[material] = []

        items = config_data[material]['items']
        lengths = config_data[material]['lengths']

        remove = None
        while len(items) > 0:

            if remove is not None:
                items.remove(remove)
                remove = None
                continue

            length = find_length(lengths, items[0]['length'])
            if length is None:
                missing_items[material].append(add_missing_item(material, items[0]))
                remove = items[0]
                continue

            if items[0]['qty'] == 0:
                remove = items[0]
                continue

            beam = Beam(material, length)

            for item in items:
                fits = beam.qty_fits(item['length'])
                if fits <= 0:
                    continue

                if fits < item['qty']:
                    use = fits
                else:
                    use = item['qty']

                beam.add_item(item=item, qty=use)

            multiplier = None

            for beam_item in beam.items:
                found_item = find_existing_item(beam_item, items)
                max = int(found_item['qty'] / beam_item['qty'])

                if multiplier is None:
                    multiplier = max

                if max < multiplier:
                    multiplier = max

            beam.qty = multiplier

            for beam_item in beam.items:
                for item in items:
                    if item['item_no'] == beam_item['item_no']:
                        item['qty'] -= beam_item['qty'] * beam.qty
                        continue

            beams_used[material].append(beam)

    return beams_used, missing_items



def find_existing_item(item, existing):
    for i in existing:
        if item['item_no'] == i['item_no']:
            return i


class Beam:
    def __init__(self, material, length):
        self.material = material
        self.length = length
        self.waste = length
        self.qty = 0
        self.items = []

    def qty_fits(self, length):
        return int(self.waste / length)

    def add_item(self, item, qty):
        new_item = {}
        new_item['length'] = item['length']
        new_item['item_no'] = item['item_no']
        new_item['qty'] = qty
        self.items.append(new_item)
        self._reduce_waste(new_item)

    def _reduce_waste(self, new_item):
        self.waste -= new_item['length'] * new_item['qty']


def add_missing_item(material, item):
    item['material'] = material
    return item

def find_length(lengths, length):
    for l in lengths:
        if l >= length:
            return l
    return None


def setup_data_required(result: BomResult):
    data = {}
    # 3 create a usable material config
    config = json.loads(result.config)
    for beam in config['beams']:
        data[beam['name']] = {'lengths': []}
        for length in beam['lengths']:
            if length['selected']:
                data[beam['name']]['lengths'].append(int(length['name']))
        temp_list = sorted(data[beam['name']]['lengths'])
        print(temp_list)
        data[beam['name']]['lengths'] = temp_list
    # 1 Getting items to be used
    items_used = get_items_to_use(result.bom_file)
    #2 totaling items required and grouping
    for key in data.keys():
        data[key]['items'] = formatted_items(items_used, key)
        temp_items = sorted(data[key]['items'], key=itemgetter('length'), reverse=True)
        data[key]['items'] = temp_items

    return data

def formatted_items(items, key):
    result = []
    data = items.get(key)

    for length in data.keys():
        new_item = {'length': float(length), 'qty': 0, 'item_no': ''}
        for item in data[length]:
            if len(new_item['item_no']) > 0:
                new_item['item_no'] = ', '.join([new_item['item_no'], item['item_no']])
            else:
                new_item['item_no'] = item['item_no']
            new_item['qty'] = new_item['qty'] + item['total_qty']
        result.append(new_item)

    return result

def get_items_to_use(bom_file: BomFile):
    """
    gets a dict of all the items required

    :param bom_file:
    :return: a dict sort by material and length
    """
    materials = {}
    materials_used = bom_file.materials_used()
    for material in materials_used:
        materials[material] = {}

    items = bom_file.contents.select(lambda item: item.length is not None)

    for item in items:
        if str(item.length) in materials[item.description].keys():
            materials[item.description][str(item.length)].append(item.for_result())
        else:
            materials[item.description][str(item.length)] = [item.for_result()]

    return materials