from flask_restful import Resource
from flask import request, json, make_response, jsonify
from pony.orm import db_session, select, desc

from common.utls import does_exist, isInt, toInt
from models.model import Project


class Projects(Resource):

    @db_session
    def get(self):
        args = request.args
        project = args.get('project')
        if project is not None:
            return getProject(project)

        resource = request.args.get('count')
        if resource is not None:
            count = 0
            if isInt(resource):
                count = toInt(resource)

            return get_project_by_count(count)

        return getAllProjects()

    @db_session
    def post(self):
        data = request.data
        data = json.loads(data)
        if valid_data(data):
            print(data)
            project = Project.add_project(data)
            data = {"message": "Project added", "data": project.to_dict(), 'status': 201}
            return make_response(jsonify(data), 201)
        else:
            return make_response(jsonify({'message': 'Resource not added', 'status': 401}), 401)


def getAllProjects():
    result = []
    projects = select(p for p in Project).order_by(Project.project_id)
    for project in projects:
        result.append(project.to_dict())
    return result


def getProject(project):
    result = select(p for p in Project if p.uuid == project)[:]
    return result[0].to_dict()


def valid_data(data):
    required = ['id', 'project', 'client']
    valid = True
    for item in required:
        valid = does_exist(data, item)
        if not valid:
            return False
    return valid

def get_project_by_count(count):
    tmp_result = select(project for project in Project).order_by(desc(Project.last_active)).limit(count)
    result = []
    for tmp in tmp_result:
        result.append(tmp.to_dict())

    return result
