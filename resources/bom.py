from flask import request
from flask_restful import Resource
from pony.orm import select, desc

from common.utls import isInt, toInt
from models.model import BomResult


class Bom(Resource):
    def get(self):
        result = []
        resource = request.args.get('count')
        if resource is not None:
            count = 0
            if isInt(resource):
                count = toInt(resource)

            result = get_boms_by_count(count)

        return {'boms': result}


def get_boms_by_count(count):
    tmp_result = select(bom for bom in BomResult).order_by(desc(BomResult.timestamp)).limit(count)
    result = []
    for tmp in tmp_result:
        result.append(tmp.table_data())

    return result
