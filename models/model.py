from collections import Counter
from datetime import timedelta, datetime
from operator import itemgetter
from statistics import mean

from pony.orm import PrimaryKey, Required, Database, Optional, Set, Json, desc

from common.utls import get_uuid
from config import config

db = Database()

class Company(db.Entity):
    id = PrimaryKey(int, auto=True)
    uuid = Required(str)
    name = Required(str)

class Project(db.Entity):
    id = PrimaryKey(int, auto=True)
    project_id = Required(str)
    name = Required(str)
    client = Required(str)
    uuid = Required(str)
    bom_results = Set('BomResult')
    created = Optional(datetime, default=lambda: datetime.utcnow())
    last_active = Optional(datetime, default=lambda: datetime.utcnow())
    archived = Optional(bool, default=False)

    def to_dict(self):
        return {'project_id': self.project_id,
                'name': self.name,
                'client': self.client,
                'uuid': self.uuid,
                'boms': len(self.bom_results)
                }

    def get_header(self):
        return {'project_id': self.project_id,
                'name': self.name,
                'client': self.client,
                }

    def get_stats(self):
        return {
            'created': self.created.strftime('%d/%m/%Y'),
            'last_active': self.last_active.strftime('%d/%m/%Y %H:%M'),
            'bom_count': self.bom_results.count(),
            'bom_created': self.last_created_bom()
        }

    def get_boms(self):
        result = []
        for bom in self.bom_results:
            result.append(bom.table_data())
        return sorted(result, key=itemgetter('created'), reverse=True)


    def last_created_bom(self):
        boms = self.bom_results.sort_by(desc(BomResult.timestamp))
        bom = boms.first()
        if bom is not None:
            return bom.timestamp.strftime('%d/%m/%Y %H:%M')
        else:
            return ""

    @staticmethod
    def add_project(data):
        project = Project(project_id=data['id'],
                          name=data['project'],
                          client=data['client'],
                          uuid=get_uuid())
        db.commit()
        return project


class BomFile(db.Entity):
    id = PrimaryKey(int, auto=True)
    project_id = Required(str)
    filename = Required(str)
    uuid = Required(str)
    contents = Set('BomFileContent')
    result = Set('BomResult')

    def materials_used(self):
        materials = []

        for content in self.contents:
            if content.is_beam():
                if content.description not in materials:
                    materials.append(content.description)
        return materials

    def set_items_parents(self):
        for content in self.contents:
            content.set_parent()
        db.commit()


class BomFileContent(db.Entity):
    id = PrimaryKey(int, auto=True)
    item_no = Optional(str)
    part_number = Optional(str)
    description = Optional(str)
    BB_length = Optional(float)
    BB_width = Optional(float)
    BB_thickness = Optional(float)
    length = Optional(float)
    qty = Optional(int)
    parent = Optional(int)
    bom_file = Required(BomFile)

    def is_beam(self):
        if self.length is not None:
            return True
        else:
            return False

    def for_result(self):
        return {
            'item_no': self.item_no,
            'description': self.description,
            'length': self.length,
            'total_qty': self.total_qty()
        }

    def total_qty(self):
        total_qty = self.qty
        if self.parent is None:
            return total_qty
        else:
            total_qty = total_qty * BomFileContent[self.parent].total_qty()
            return total_qty

    def set_parent(self):
        parent_no = self._get_parent_no()
        self.parent = self._get_parent_id(parent_no)

    def _get_parent_id(self, parent_no):
        parent = self.bom_file.contents.select(lambda content: content.item_no == parent_no).first()
        if parent is not None:
            return parent.id
        else:
            return None

    def _get_parent_no(self):
        parent = self.item_no.split(".")
        if len(parent) == 1:
            return None
        else:
            return ".".join(parent[:-1])

class Library_beam(db.Entity):
    id = PrimaryKey(int, auto=True)
    uuid = Optional(str)
    display_name = Optional(str)
    aliases = Set('Library_beam_alias')
    lengths = Set('Library_beam_length')

    def to_dict(self):
        output = {
            'name': self.display_name,
            'id': self.uuid,
            'lengths': []
        }

        for length in self.lengths:
            output['lengths'].append(length.to_dict())

        return output

    def for_display(self):
        output = {
            'name': self.display_name,
            'key': self.uuid,
            'lengths': [],
            'alias': []
        }

        for length in self.lengths:
            output['lengths'].append(length.length)

        for alias in self.aliases:
            output['alias'].append(alias.description)

        output['lengths'] = sorted(output['lengths'])
        output['alias'] = sorted(output['alias'])
        return output


class Library_beam_alias(db.Entity):
    id = PrimaryKey(int, auto=True)
    description = Optional(str)
    beam = Optional('Library_beam')


class Library_beam_length(db.Entity):
    id = PrimaryKey(int, auto=True)
    uuid = Optional(str)
    length = Optional(int)
    beams = Set('Library_beam')

    def to_dict(self):
        return {
            'name': self.length,
            'id': self.uuid,
        }


class BomResult(db.Entity):
    id = PrimaryKey(int, auto=True)
    uuid = Optional(str)
    archived = Optional(bool, default=False)
    filename = Optional(str)
    description = Optional(str)
    process_time = Optional(timedelta)
    timestamp = Optional(datetime)
    project = Required(Project)
    bom_file = Required(BomFile)
    bom_result_sections = Set('BomResultSection')
    config = Optional(Json)

    def to_dict(self):
        result = {}
        result['project'] = self.project.to_dict()
        result['bom'] = {}
        result['bom']['header'] = self.result_header()
        result['bom']['beams'] = self.result_beams()
        result['bom']['result_table'] = self.result_table()
        result['bom']['missing_table'] = self.missing_table()

        return result

    def missing_table(self):

        result = []
        for section in self.bom_result_sections:
            result = result + section.missing_parts_for_table()
        return result

    def result_header(self):
        return {
            'date': self.timestamp.strftime('%d/%m/%Y %H:%M'),
            'description': self.description,
            'filename': self.filename
        }

    def result_beams(self):
        result = []
        for section in self.bom_result_sections:
            result.append({
                'name': section.name,
                'beams': section.beam_to_dict(),
                'key': get_uuid()
            })
        return result

    def result_table(self):
        result = {
            'columns': self.get_result_table_columns(),
            'dataSource': self.get_result_table_data()
        }
        return result

    def get_result_table_columns(self):
        # {
        #     title: '6100',
        #     dataIndex: 'di6100',
        #     key: 'di6100',
        #     align: 'center'
        # },
        result = []
        for section in self.bom_result_sections:
            for beam in section.beams:
                if beam.length not in [item.get('title') for item in result]:
                    result.append({
                        'title': beam.length,
                        'dataIndex': f'di{beam.length}',
                        'key': f'di{beam.length}',
                        'align': 'center',
                    })

        result = sorted(result, key=itemgetter('title'))
        return result

    def get_result_table_data(self):
        result = []

        for section in self.bom_result_sections:
            result.append(section.stats())
        return result

    def table_data(self):
        tags = []
        if self.has_missing():
            tags.append('missing parts')

        return {
            'key': self.uuid,
            'created': self.timestamp.strftime('%d/%m/%Y %H:%M'),
            'description': self.description,
            'tags': tags
        }

    def has_missing(self):
        for section in self.bom_result_sections:
            if len(section.missing_parts) > 0:
                return True
        else:
            return False

class BomResultSection(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Optional(str)
    bom_result = Required(BomResult)
    beams = Set('BomResultBeam')
    missing_parts = Set('BomResultMissingPart')

    def beam_to_dict(self):
        result = []
        for beam in self.beams:
            result.append(beam.to_dict())

        result = sorted(result, key=itemgetter('percent'), reverse=True)
        return result

    def missing_parts_for_table(self):
        result = []
        for part in self.missing_parts:
            result.append(part.display())
        return result

    def has_missing_part(self):
        if len(self.missing_parts) > 0:
            return 'missing'
        else:
            return 'ok'

    def stats(self):
        # {
        #     key: '1',
        #     material: '300x300x12.50 SHS',
        #     average: '85%',
        #     high: '100%',
        #     low: '39%',
        #     di12000: 14,
        #     di14000: 1
        # },
        temp_stats = {
            'key': get_uuid(),
            'material': self.name,
            'average': self.average_usage(),
            'high': self.high_usage(),
            'low': self.low_usage(),
            'status': self.has_missing_part()
        }

        length_ids = self.get_length_id_count()

        stats = {**temp_stats, **length_ids}

        return stats

    def get_length_id_count(self):
        result = {}
        lengths = [beam.length for beam in self.beams]
        counted = Counter(lengths)
        counted = dict(counted)

        for key in counted.keys():
            result[f'di{key}'] = counted[key]

        return result

    def high_usage(self):
        max = None
        for beam in self.beams:
            percent = beam.get_percent()
            if max is None:
                max = percent

            if percent > max:
                max = percent

        return max

    def low_usage(self):
        min = None
        for beam in self.beams:
            percent = beam.get_percent()
            if min is None:
                min = percent

            if percent < min:
                min = percent

        return min

    def average_usage(self):
        percents = [beam.get_percent() for beam in self.beams]
        avg = int(mean(percents))
        return avg

class BomResultBeam(db.Entity):
    id = PrimaryKey(int, auto=True)
    qty = Optional(int)
    length = Optional(int)
    waste = Optional(int)
    section = Required(BomResultSection)
    parts = Set('BomResultBeamPart')

    def to_dict(self):
        result = {
            'qty': self.qty,
            'length': self.length,
            'waste': self.waste,
            'parts': [],
            'percent': self.get_percent(),
            'usage': self.length - self.waste,
            'key': get_uuid()
        }
        for part in self.parts:
            result['parts'].append(part.to_dict())

        result['parts'] = sorted(result['parts'], key=itemgetter('length'), reverse=True)
        return result

    def get_percent(self):
        return int(( (self.length - self.waste) / self.length) * 100)

class BomResultBeamPart(db.Entity):
    id = PrimaryKey(int, auto=True)
    item_no = Optional(str)
    length = Optional(float)
    qty = Optional(int)
    beam = Optional(BomResultBeam)

    def to_dict(self):
        return {
            'item_no': self.item_no,
            'qty': self.qty,
            'length': self.length,
            'key': get_uuid()
        }

class BomResultMissingPart(db.Entity):
    id = PrimaryKey(int, auto=True)
    item_no = Optional(str)
    length = Optional(float)
    qty = Optional(int)
    section = Required(BomResultSection)

    def display(self):
        # {
        #     key: '1',
        #     material: 'UC 356x406x287',
        #     item: '2.4.3',
        #     length: 2000,
        #     qty: 2
        # },
        return {
            'key': get_uuid(),
            'material': self.section.name,
            'item': self.item_no,
            'length': self.length,
            'qty': self.qty
        }

db.bind(config['database'])
db.generate_mapping(create_tables=True)